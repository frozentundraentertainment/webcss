package main

import (
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v2"
)

func writeYaml(yamlFile string, javascriptSRI string, cssSRI string, cssCRC string) error {
	log.Debug().
		Str("Func", "writeYaml").
		Str("Output File", yamlFile).
		Msg("Saving YAML config")

	dest, err := os.Create(yamlFile)
	if err != nil {
		return fmt.Errorf("creating YAML config file [%s]: %w", yamlFile, err)
	}
	defer fileCloserFunc(dest, dest.Name())

	encoder := yaml.NewEncoder(dest)

	type asset struct {
		SRI string
		CRC string
	}

	type config struct {
		Javascript asset
		CSS        asset
	}

	err = encoder.Encode(config{
		Javascript: asset{
			SRI: javascriptSRI,
		},
		CSS: asset{
			SRI: cssSRI,
			CRC: cssCRC,
		},
	})
	if err != nil {
		return fmt.Errorf("encoding YAML to file: %w", err)
	}

	return nil
}

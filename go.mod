module webCSS

go 1.16

require (
	github.com/rs/zerolog v1.21.0
	github.com/wellington/go-libsass v0.9.2
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

package main

import (
	"crypto/sha512"
	"encoding/hex"
	"fmt"
	"hash/crc32"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"github.com/rs/zerolog/log"
)

// **********************

// Helper functions

// **********************

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
// func fileExists(filename string) error {
// 	info, err := os.Stat(filename)
// 	if os.IsNotExist(err) {
// 		return err
// 	}
// 	if info.IsDir() {
// 		return fmt.Errorf("is directory")
// 	}
// 	return nil
// }
//
// func writeFile(path string, content []byte) error {
// 	f, err := os.Create(path)
// 	if err != nil {
// 		return err
// 	}
// 	defer fileCloserFunc(f, f.Name())
// 	_, err = f.Write(content)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }
//
// func isEmpty(name string) (bool, error) {
// 	f, err := os.Open(name)
// 	if err != nil {
// 		return false, err
// 	}
// 	defer fileCloserFunc(f, f.Name())
// 	_, err = f.Readdirnames(1) // Or f.Readdir(1)
// 	if err == io.EOF {
// 		return true, nil
// 	}
// 	return false, err // Either not empty or error, suits both cases
// }
//
// func findNewestFile(dir string) (os.FileInfo, error) {
// 	files, err := ioutil.ReadDir(dir)
// 	if err != nil {
// 		return nil, fmt.Errorf("could not read dir(%s): %w", dir, err)
// 	}
// 	newestTime := time.Unix(0, 0)
// 	var newestFile os.FileInfo = nil
//
// 	for _, file := range files {
// 		if file.Mode().IsDir() {
// 			// recursive call to check files and subdirectories
// 			file, err = findNewestFile(filepath.FromSlash(dir + "/" + file.Name()))
// 			if err != nil {
// 				return nil, fmt.Errorf("could not read recursive dir(%s): %w", dir, err)
// 			}
// 		}
//
// 		if file.Mode().IsRegular() && file.ModTime().After(newestTime) {
// 			newestFile = file
// 			newestTime = file.ModTime()
// 		}
// 	}
// 	if newestFile == nil {
// 		// err = os.ErrNotExist
// 		return nil, fmt.Errorf("no newer file found")
// 	}
// 	return newestFile, nil
// }

func fileCloserFunc(file http.File, name string) {
	if err := file.Close(); err != nil {
		log.Error().Err(err).Str("File", name).Msg("Could not defer close the file.")
	}
}

// copyRecursive copies src to dest, doesn't matter if src is a directory or a file
// If it is a directory, it will copy children as well.
func copyRecursive(src, dest string) error {
	info, err := os.Lstat(src)
	if err != nil {
		return fmt.Errorf("could not Lstat the file: %w", err)
	}
	return copyPath(src, dest, info)
}

// copyPath dispatches copyPath-funcs according to the mode.
// Because this "copyPath" could be called recursively,
// "info" MUST be given here, NOT nil.
func copyPath(src, dest string, info os.FileInfo) error {
	if info == nil {
		return fmt.Errorf("copyPath - os.FileInfo must not be nil")
	}
	if info.Mode()&os.ModeSymlink != 0 {
		return copyLink(src, dest, info)
	}
	if info.IsDir() {
		return copyDirectory(src, dest, info)
	}
	return copyFile(src, dest, info)
}

// copyFile is for just a file,
// with considering existence of parent directory
// and file permission.
func copyFile(src, dest string, info os.FileInfo) error {
	if err := os.MkdirAll(filepath.Dir(dest), os.ModePerm); err != nil {
		return fmt.Errorf("could not make the destination directory: %w", err)
	}
	f, err := os.Create(dest)
	if err != nil {
		return fmt.Errorf("could not create the destination file: %w", err)
	}
	defer fileCloserFunc(f, f.Name())
	if err = os.Chmod(f.Name(), info.Mode()); err != nil {
		return fmt.Errorf("could not set destination file permissions: %w", err)
	}
	s, err := os.Open(src)
	if err != nil {
		return fmt.Errorf("could not open source file: %w", err)
	}
	defer fileCloserFunc(s, s.Name())
	if _, err = io.Copy(f, s); err != nil {
		return fmt.Errorf("could not copy file: %w", err)
	}

	return nil
}

// copyDirectory is for a directory,
// with scanning contents inside the directory
// and pass everything to "copyPath" recursively.
func copyDirectory(srcDir, destDir string, info os.FileInfo) error {
	if err := os.MkdirAll(destDir, info.Mode()); err != nil {
		return fmt.Errorf("could not make the destination directory: %w", err)
	}
	contents, err := ioutil.ReadDir(srcDir)
	if err != nil {
		return fmt.Errorf("could not read the source directory: %w", err)
	}
	for _, content := range contents {
		cs, cd := filepath.Join(srcDir, content.Name()), filepath.Join(destDir, content.Name())
		if err = copyPath(cs, cd, content); err != nil {
			// If any error, exit immediately
			return fmt.Errorf("could not copy directory contents to new location: %w", err)
		}
	}
	return nil
}

// copyLink is for a symlink,
// with just creating a new symlink by replicating src symlink.
func copyLink(src, dest string, _ os.FileInfo) error {
	src, err := os.Readlink(src)
	if err != nil {
		return fmt.Errorf("could not copy link: %w", err)
	}
	return os.Symlink(src, dest)
}

func generateSRI(sourceFile string) (sri string, err error) {
	log.Debug().
		Str("Func", "generateSRI").
		Str("sourceFile", sourceFile).
		Msg("Generating SRI")

	// Open file for hashing
	f, err := os.Open(sourceFile)
	if err != nil {
		return "", fmt.Errorf("could not open given asset file: %w", err)
	}

	// Close file on exit and check for its returned error
	defer fileCloserFunc(f, f.Name())

	// Specify Hash function
	h := sha512.New384()

	// Copy file to hasher
	if _, err = io.Copy(h, f); err != nil {
		return "", fmt.Errorf("could not import file to hasher: %w", err)
	}

	// Calculate SRI of file
	return fmt.Sprintf("sha384-%x", h.Sum(nil)), nil
}

func hashFileCrc32(sourceFile string, polynomial uint32) (crc string, err error) {
	log.Debug().
		Str("Func", "hashFileCrc32").
		Str("sourceFile", sourceFile).
		Uint32("polynomial", polynomial).
		Msg("Compiling Javascript")

	// Open the file for hashing
	file, err := os.Open(sourceFile)
	if err != nil {
		return "", fmt.Errorf("could not open file [%s] to hash: %w", sourceFile, err)
	}

	// Close dstFile on exit and check for its returned error
	defer fileCloserFunc(file, file.Name())

	// Calculate hash of file
	hash := crc32.New(crc32.MakeTable(polynomial))
	if _, err = io.Copy(hash, file); err != nil {
		return "", fmt.Errorf("could not calculate the crc32 of the file: %w", err)
	}

	return hex.EncodeToString(hash.Sum(nil)[:]), nil
}

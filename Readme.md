When downloading this repository for the first time, ensure to download the submodules as well.

```git submodule update --init --recursive```

To update the submodule to the most recent version:

```git submodule update --remote --merge```

To change the version of a submodule to a specific tag

```git -C src/bootstrap checkout v4.6.0```

package main

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// Download all submodules
// //go:generate git submodule update --init --recursive
// Update all submodules to latest copy
// //go:generate git submodule update --recursive --remote --merge
// Change to the specified Bootstrap Version
// //go:generate git -C src/bootstrap checkout v4.6.0
// Download Bootstrap source
//go:generate git clone -b v4.6.0 https://github.com/twbs/bootstrap.git src/bootstrap
// Download package dependencies
//go:generate go mod download
// Tidy up Go Modules, add/remove modules needed in package
//go:generate go mod tidy
// Verify our Go Modules cache
//go:generate go mod verify
// Precompile go-libsass
// //go:generate go install github.com/wellington/go-libsass
// Build the assets
//go:generate go run .

const (
	javascriptSrc  = "src/bootstrap/dist/js"
	javascriptDir  = "assets/js"
	javascriptName = "bootstrap.bundle.min"
	scssSrc        = "src/appSASS/app.scss"
	cssDest        = "assets/css/app"
	yamlDest       = "assets/metadata.yaml"
)

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, NoColor: true})
}

func main() {
	// Process Javascript
	jsSri, err := compileJavascript(javascriptSrc, javascriptDir, javascriptName)
	if err != nil {
		log.Fatal().Stack().Err(err).
			Str("Source", javascriptSrc).
			Str("Directory", javascriptDir).
			Str("FileName", javascriptName).
			Msg("Failed to compile the Javascript")
	}

	// Process SASS
	cssCrc, cssSri, err := compileCSS(scssSrc, cssDest)
	if err != nil {
		log.Fatal().Stack().Err(err).
			Str("Source", scssSrc).
			Str("Directory", cssDest).
			Msg("Failed to compile the SCSS")
	}

	if err := writeYaml(yamlDest, jsSri, cssSri, cssCrc); err != nil {
		log.Fatal().Stack().Err(err).
			Str("YAML File", yamlDest).
			Str("Javascript SRI", jsSri).
			Str("CSS SRI", cssSri).
			Str("CSS CRC", cssCrc).
			Msg("Failed to write the asset metadata")
	}

	log.Info().
		Str("Javascript SRI", jsSri).
		Str("CSS SRI", cssSri).
		Str("CSS CRC", cssCrc).
		Msg("Compilation Complete!")
}

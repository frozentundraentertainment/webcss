package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"github.com/wellington/go-libsass"
)

// compileCSS will take the given root SCSS/SASS file (scssFile) and compile it to a versioned CSS file
// outFullPath should be the complete filepath, including filename, without a file extension
func compileCSS(scssFile string, outFullPath string) (crc string, sri string, err error) {
	log.Debug().
		Str("Func", "compileCSS").
		Str("scssFile", scssFile).
		Str("outFullPath", outFullPath).
		Msg("Compiling CSS")

	// Check if variables are set
	if scssFile == "" {
		return "", "", fmt.Errorf("scss source file not set")
	}
	if outFullPath == "" {
		return "", "", fmt.Errorf("compiled CSS directory not set")
	}

	compiledCSSFile := fmt.Sprintf("%s.css", outFullPath)
	if err := compileSASS(scssFile, compiledCSSFile); err != nil {
		return "", "", fmt.Errorf("could not compile SASS: %w", err)
	}

	log.Debug().Str("CSS File", compiledCSSFile).Msg("Calculating CSS version")
	crc, err = versionCSS(compiledCSSFile, outFullPath)
	if err != nil {
		return "", "", fmt.Errorf("could not version the compiled css: %w", err)
	}

	finalPath := fmt.Sprintf("%s.%s.css", outFullPath, crc)

	log.Debug().Str("CSS File", finalPath).Msg("Calculating CSS SRI")
	sri, err = generateSRI(finalPath)
	if err != nil {
		return "", "", fmt.Errorf("could not generate the CSS SRI: %w", err)
	}

	return crc, sri, nil
}

func versionCSS(sourceFile string, destinationFile string) (crc string, err error) {
	log.Debug().
		Str("Func", "versionCSS").
		Str("sourceFile", sourceFile).
		Str("destinationFile", destinationFile).
		Msg("Versioning CSS")

	crc, err = hashFileCrc32(sourceFile, 0xedb88320)
	if err != nil {
		return "", fmt.Errorf("could not generate a crc32 for [%s]: %w", sourceFile, err)
	}

	// Save versioned CSS file
	outFile := fmt.Sprintf("%s.%s.css", destinationFile, crc)

	log.Info().Str("Destination File", outFile).Msg("Saving versioned CSS")
	return crc, copyRecursive(sourceFile, outFile)
}

func compileSASS(sassSrc string, cssFile string) error {
	log.Debug().
		Str("Func", "compileSASS").
		Str("SASS Source file", sassSrc).
		Str("CSS Output File", cssFile).
		Msg("Compiling SASS to CSS")

	src, err := os.Open(sassSrc)
	if err != nil {
		return fmt.Errorf("opening SASS source file [%s]: %w", sassSrc, err)
	}
	defer fileCloserFunc(src, src.Name())

	if err := os.MkdirAll(filepath.Dir(cssFile), 0550); err != nil {
		return fmt.Errorf("could not create the destination directory: %w", err)
	}

	dest, err := os.Create(cssFile)
	if err != nil {
		return fmt.Errorf("creating CSS destination file [%s]: %w", cssFile, err)
	}
	defer fileCloserFunc(dest, dest.Name())

	compiler, err := libsass.New(dest, src)
	if err != nil {
		return fmt.Errorf("initilizing LibSASS: %w", err)
	}

	// TODO: read given scss file and interpolate additional paths
	if err = compiler.Option(libsass.IncludePaths([]string{
		"src/appSASS",
		"src/bootstrap/scss",
	})); err != nil {
		return fmt.Errorf("adding additional search paths: %w", err)
	}

	if err := compiler.Run(); err != nil {
		return fmt.Errorf("compiling SASS to CSS: %w", err)
	}

	return nil
}

package main

import (
	"fmt"
	"path/filepath"

	"github.com/rs/zerolog/log"
)

func compileJavascript(inPath string, outPath string, outName string) (sri string, err error) {
	log.Debug().
		Str("Func", "compileJavascript").
		Str("inPath", inPath).
		Str("outPath", outPath).
		Str("outName", outName).
		Msg("Compiling Javascript")

	if inPath == "" {
		return "", fmt.Errorf("javascript input path not set")
	}
	if outPath == "" {
		return "", fmt.Errorf("javascript output path not set")
	}
	if outName == "" {
		return "", fmt.Errorf("javascript output filename not set")
	}

	if err := copyRecursive(inPath, outPath); err != nil {
		return "", fmt.Errorf("could not recursively copy Javascript files: %w", err)
	}

	// Calculate SRI Hash of Javascript files
	outFile := fmt.Sprintf("%s.js", filepath.Join(outPath, outName))
	log.Info().Str("Javascript File", outFile).Msg("Calculating Javascript SRI")
	return generateSRI(outFile)
}
